﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace andrei.Entities
{
    public class Professor
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
    }
}
