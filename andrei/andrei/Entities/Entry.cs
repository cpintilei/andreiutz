﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace andrei.Entities
{
    public class Entry
    {
        public int OwnerId { get; set; }

        public int Id { get; set; }
        public string Type { get; set; }
        public string ProfessorName { get; set; }
        public string Title { get; set; }
    }
}
