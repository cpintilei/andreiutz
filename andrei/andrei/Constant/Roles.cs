﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace andrei.Constant
{
    public static class Roles
    {
        public readonly static string Administrator = "Administrator";
        public readonly static string Student = "Student";
        public readonly static string Professor = "Professor";

    }
}
