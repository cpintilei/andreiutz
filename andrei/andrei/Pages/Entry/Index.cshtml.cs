﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using andrei.Data;
using andrei.Models;
using andrei.Constant;

namespace andrei.Pages.Entry
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public IndexModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<EntryModel> EntryModel { get; set; }

        public async Task OnGetAsync()
        {
            if (HttpContext.User.IsInRole(Roles.Professor))
            {
                var id = _context.Users.FirstOrDefault(x => x.Email == HttpContext.User.Identity.Name).Id;
                var professors = _context.Professors.ToList();
                var prof = professors.FirstOrDefault(y => y.UserId == id);

                EntryModel = await _context.Entry.Where(x=>x.ProfessorName==prof.UserId).Select(x => new EntryModel() { OwnerEmail = x.OwnerEmail, Title = x.Title, Type = x.Type, ProfessorName = prof.Name }).ToListAsync();
            }            
        }
    }
}
