﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using andrei.Data;
using andrei.Models;

namespace andrei.Pages.Entry
{
    public class DetailsModel : PageModel
    {
        private readonly andrei.Data.ApplicationDbContext _context;

        public DetailsModel(andrei.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public EntryModel EntryModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EntryModel = await _context.Entry.FirstOrDefaultAsync(m => m.Id == id);

            if (EntryModel == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
