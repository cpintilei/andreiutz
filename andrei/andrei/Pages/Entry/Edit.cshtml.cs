﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using andrei.Data;
using andrei.Models;

namespace andrei.Pages.Entry
{
    public class EditModel : PageModel
    {
        private readonly andrei.Data.ApplicationDbContext _context;

        public EditModel(andrei.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public EntryModel EntryModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EntryModel = await _context.Entry.FirstOrDefaultAsync(m => m.Id == id);

            if (EntryModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(EntryModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntryModelExists(EntryModel.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool EntryModelExists(int id)
        {
            return _context.Entry.Any(e => e.Id == id);
        }
    }
}
