﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using andrei.Data;
using andrei.Models;

namespace andrei.Pages.Entry
{
    public class CreateModel : PageModel
    {
        private readonly andrei.Data.ApplicationDbContext _context;

        public CreateModel(andrei.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public EntryModel EntryModel { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            EntryModel.OwnerEmail= HttpContext.User.Identity.Name;

            _context.Entry.Add(EntryModel);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Index");
        }
    }
}