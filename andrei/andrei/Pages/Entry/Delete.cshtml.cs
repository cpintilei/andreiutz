﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using andrei.Data;
using andrei.Models;

namespace andrei.Pages.Entry
{
    public class DeleteModel : PageModel
    {
        private readonly andrei.Data.ApplicationDbContext _context;

        public DeleteModel(andrei.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public EntryModel EntryModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EntryModel = await _context.Entry.FirstOrDefaultAsync(m => m.Id == id);

            if (EntryModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EntryModel = await _context.Entry.FindAsync(id);

            if (EntryModel != null)
            {
                _context.Entry.Remove(EntryModel);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
