﻿using andrei.Data;
using andrei.Entities;
using andrei.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace andrei.Service
{
    public class EntryOptionsService
    {
        private readonly ApplicationDbContext context;

        public EntryOptionsService(ApplicationDbContext con)
        {
            this.context = con;
        }

        public List<string> ListTypes()
        {
            return new List<string>()
            {
                "Bachelor",
                "Master",
                "PhD"
            };
        }

        public List<Professor> ListProfessors()
        {
            var professors = this.context.Professors.ToList();

            return professors;
        }
    }
}
