﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace andrei.Models
{
    public class ProfessorModel
    {
        public int ProfessorId { get; set; }
        public string Name { get; set; }
    }
}
