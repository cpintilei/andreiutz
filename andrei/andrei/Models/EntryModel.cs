﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace andrei.Models
{
    public class EntryModel
    {
        public string OwnerId { get; set; }
        public string OwnerEmail { get; set; }

        public int Id { get; set; }
        public string Type { get; set; }

        [Display(Name ="Professor Name")]
        public string ProfessorName { get; set; }
        public string Title { get; set; }
    }
}
