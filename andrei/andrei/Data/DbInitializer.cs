﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using andrei.Entities;
using Microsoft.AspNetCore.Identity;

namespace andrei.Data
{
    public static class DbInitializer
    {
        public static void Initialize()
        {

        }

        internal static void Initialize(ApplicationDbContext context)
        {
            var professors = context.Users.ToList();

            if (context.Professors.Any())
                return;

            var prof1 = professors.FirstOrDefault(x => x.Email.Contains("professor1"));
            var prof2 = professors.FirstOrDefault(x => x.Email.Contains("professor2"));

            context.Professors.Add(new Professor()
            {
                Name="prof1",
                UserId=prof1.Id
            });

            context.Professors.Add(new Professor()
            {
                Name = "prof2",
                UserId = prof2.Id
            });

            context.SaveChanges();
        }
    }
}
