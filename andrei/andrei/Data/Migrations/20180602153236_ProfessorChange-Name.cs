﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace andrei.Data.Migrations
{
    public partial class ProfessorChangeName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Professors");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Professors",
                newName: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Professors",
                newName: "LastName");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Professors",
                nullable: true);
        }
    }
}
