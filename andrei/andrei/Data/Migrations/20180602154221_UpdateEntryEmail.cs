﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace andrei.Data.Migrations
{
    public partial class UpdateEntryEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OwnerEmail",
                table: "Entry",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerEmail",
                table: "Entry");
        }
    }
}
