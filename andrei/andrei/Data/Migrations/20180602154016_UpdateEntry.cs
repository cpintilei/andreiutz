﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace andrei.Data.Migrations
{
    public partial class UpdateEntry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OwnerId",
                table: "Entry",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Entry",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
